package com.codepath.mypizza.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.codepath.mypizza.R
import com.codepath.mypizza.data.Pizza

/**
 * Created by Shyam Rokde on 8/5/16.
 */
class PizzaDetailFragment : Fragment() {
    internal var position = 0
    internal lateinit var bundle: Bundle
    internal lateinit var tvTitle: TextView
    internal lateinit var tvDetails: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        position = arguments?.getInt("position", 0)  ?: 0
        Log.d(TAG,"updateView pos "+position )
    }

    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, savedInstanceState: Bundle?): View? {

        // Inflate the xml file for the fragment
        Log.d(TAG,"updateView pos "+position + " " + Pizza.pizzaMenu[position])
        return inflater.inflate(R.layout.fragment_pizza_detail, parent, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Set values for view here


        tvTitle = view.findViewById<View>(R.id.tvTitle) as TextView
        tvDetails = view.findViewById<View>(R.id.tvDetails) as TextView

        // update view
        tvTitle.text = Pizza.pizzaMenu[position]
        tvDetails.text = Pizza.pizzaDetails[position]
        Log.d(TAG,"onViewCreated pos "+position + " " + Pizza.pizzaMenu[position])

    }

    // Activity is calling this to update view on Fragment
    fun updateView(position: Int) {
        tvTitle.text = Pizza.pizzaMenu[position]
        tvDetails.text = Pizza.pizzaDetails[position]
        Log.d(TAG,"updateView pos "+position + " " + Pizza.pizzaMenu[position])
    }

    companion object {
        val TAG = "DETAILFRAG"
    }
}
